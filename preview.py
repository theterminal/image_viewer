import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk

# Создаем окно
window = tk.Tk()

# Функция открытия файла
def open_file():
    # Открываем диалог выбора файла
    file_path = filedialog.askopenfilename()
    # Открываем изображение
    image = Image.open(file_path)
    # Создаем объект PhotoImage изображения
    photo = ImageTk.PhotoImage(image)
    # Создаем метку и отображаем изображение
    label = tk.Label(window, image=photo)
    label.image = photo
    label.pack()

# Создаем кнопку "Открыть файл"
button = tk.Button(window, text="Открыть файл", command=open_file)
button.pack()

# Запускаем главный цикл обработки событий
window.mainloop()
